import { IsNotEmpty, MinLength, IsPositive, Matches } from 'class-validator';
export class CreateCustomerDto {
  @MinLength(3)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  age: number;

  @Matches(/^\d{10}$/)
  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  gender: string;
}
