import {
  IsNotEmpty,
  IsPositive,
  IsArray,
  ValidateNested,
} from 'class-validator';
class CreatedOrderItemDto {
  @IsPositive()
  @IsNotEmpty()
  productId: number;

  @IsPositive()
  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsPositive()
  @IsNotEmpty()
  customerId: number;

  @ValidateNested()
  @IsArray()
  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
